/* Copyright 2017 Gianmarco Garrisi

This file is part of LCS.

LCS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LCS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LCS.  If not, see <http://www.gnu.org/licenses/>. */

use std::borrow::Borrow;
use std::cmp::Eq;
use std::hash::Hash;

use ordermap::OrderMap;

use priority_queue::PriorityQueue;

pub enum CachingPolicy {
    LeastRecentlyUsed,
    LeastFrequentlyUsed,
    Random,
}

pub struct Cache<K, V> {
    cache: OrderMap<K, V>,
    replacement_order: PriorityQueue<usize, u32>,
    policy: CachingPolicy,
    max_size: usize,
    dtime: u32 //For LRU caching
}

impl<K, V> Cache<K, V>
        where K: Eq + Hash {
    pub fn new(max_size: usize, policy: CachingPolicy) -> Cache<K, V> {
        Cache{
            cache: OrderMap::with_capacity(max_size),
            replacement_order: PriorityQueue::with_capacity(max_size),
            policy: policy,
            max_size: max_size,
            dtime: u32::max_value()
        }
    }

    pub fn insert(&mut self, key: K, value: V) {
        if self.cache.contains_key(&key){
            return;
        }
        match self.policy {
            CachingPolicy::LeastFrequentlyUsed => {
                if self.cache.len() == self.max_size {
                    self.cache.swap_remove_index(self.replacement_order.pop().unwrap().0);
                }
                let i = self.cache.len();
                self.cache.insert(key, value);
                self.replacement_order.push(i, u32::max_value());
            },
            CachingPolicy::LeastRecentlyUsed => {
                if self.cache.len() == self.max_size {
                    self.cache.swap_remove_index(self.replacement_order.pop().unwrap().0);
                } 
                let i = self.cache.len();
                self.cache.insert(key, value);
                self.replacement_order.push(i, self.dtime);
                self.dtime -=1;
            },
            CachingPolicy::Random => unimplemented!()
        }
    }

    pub fn get<Q: ?Sized>(&mut self, key: &Q) -> Option<&V>
        where K: Borrow<Q>,
              Q: Hash+Eq {
        if let Some((idx, _, v)) = self.cache.get_pair_index(key){
            match self.policy{
                CachingPolicy::LeastFrequentlyUsed => {
                    self.replacement_order.change_priority_by(&idx, |p| p-1);
                },
                CachingPolicy::LeastRecentlyUsed => {
                    self.replacement_order.change_priority(&idx, self.dtime);
                    self.dtime -= 1;
                },
                CachingPolicy::Random => unimplemented!()
            }
            Some(v)
        } else {
            None
        }
    }
}
