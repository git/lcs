/* Copyright 2017 Gianmarco Garrisi

This file is part of LCS.

LCS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LCS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LCS.  If not, see <http://www.gnu.org/licenses/>. */

//! This module contains the facade class for the DMX system
//! should offer an elegant interface

use std::option::Option;
use std::result::Result;
use std::time::Duration;

use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};

use std::thread;

use std::fs::File;
use std::path::Path;
use std::io::{Read, Result as IOResult};


use serial::PortSettings;

use ordermap::OrderMap;

use theban_interval_tree::IntervalTree;
use memrange::Range;

use dmxsystem::devs::*;
use dmxsystem::upthread::{Updater, UpThread};
use ::ChVal;
use ::Transition;

/// This struct is the facade of the DMS system. Through its methods
/// is possible to control every funtionality offered by the system.
#[derive(Debug)]
pub struct Universe {
    //deletion can be logically handled with Option
    names:    OrderMap<String, ()>,
    used_chs: IntervalTree<usize>,
    lights:   Vec<Arc<SimpleLight>>,
    colors:   Vec<Option<ColorLight>>,
    dimmers:  Vec<Option<Dimmer>>,
    updater:  Option<UpThread>,
}

impl Universe {

    /// Create a new, default universe
    pub fn new() -> Universe {
        Universe{
            names:    OrderMap::new(),
            used_chs: IntervalTree::new(),
            lights:   Vec::new(),
            colors:   Vec::new(),
            dimmers:  Vec::new(),
            updater:  None,
        }
    }

    //lookup serde for store and load
    /// Load a universe from a file
    pub fn load<P: AsRef<Path>>(path: P) -> IOResult<Universe> {
        let u = Self::new();
        let mut file = try!(File::open(path));
        let mut s = String::new();
        try!(file.read_to_string(&mut s));
        for l in s.lines(){

        }
        Ok(u)
    }

  // pub fn save<P: AsRef<Path>>(&self, path: P) -> IOResult<()> {
  //     let mut file = try!(File::create(path));
  //
  // }

    /// Sets and starts the updater thread that performs
    /// the communication to the DMX line
    pub fn start(&mut self, settings: PortSettings )
                 -> Result<(), ::serial::Error> {
        let ut = Updater::set(self.lights.clone(), settings).start()?;
        self.updater = Some(ut);
        Ok(())
    }

    /// Stops the updater thread
    pub fn stop(&mut self) -> Result<(), String> {
        if let Some(c) = self.updater.take() {
            c.stop();
            Ok(())
        } else {
            Err("System is not running".to_string())
        }
    }

    /// Add a light to the system
    pub fn add_light(&mut self, name: String, first_ch: u16
                     , number_of_channels: u16) -> Result<usize, String>{
        // println!("{:?} first_ch: {:?}, {:?} channels",
        //          name, first_ch, number_of_channels);
        if self.updater.is_some() {
            return Err("System started.
It's not allowed to change the configuration".to_string());
        }
        let idx = self.names.len();
        if self.names.contains_key(&name) {
            return Err("Name not unique".to_string());
        }
        if self.used_chs
            .range(first_ch as u64,(first_ch+number_of_channels-1) as u64)
            .count() > 0 {
                return Err(format!("Intersection of channels"));
            };
        self.names.insert(name, ());
        let new_light = Arc::new(
            SimpleLight::new(self.lights.len(), first_ch, number_of_channels));
        self.used_chs.insert(
            Range::new(first_ch as u64,(first_ch+number_of_channels-1) as u64),
            idx);
        self.lights.push(new_light.clone());
        self.dimmers.push(None);
        self.colors.push(None);
        Ok(idx)
    }

    /// Get the stored name for the light identifier provided
    pub fn get_name(&self, idx:usize) -> Option<&str> {
        self.names.get_index(idx).map(|p| p.0.as_str())
    }

    /// Get the index of the light identified by the provided name
    pub fn get_index(&self, name: &str) -> Option<usize> {
        self.names.get_pair_index(name).map(|(idx, _, _)| idx)
    }

    /// Get the light identified by the provided name
    pub fn get_light(&self, name: &str) -> Option<Arc<SimpleLight>> {
        self.get_index(name).map(|i| { self.lights[i].clone() })
    }

    /// Get the light at the provided index
    pub fn get_light_by_idx(&self, idx: usize) -> Option<Arc<SimpleLight>> {
        if self.lights.len() > idx {
            Some(self.lights[idx].clone())
        } else {
            None
        }
    }

    /// Add a coarse dimmer on the light at the provided index
    pub fn add_dimmer(&mut self, id: usize, dimmer_ch: u16){
        let dimmer = Dimmer::new(self.lights[id].clone(), dimmer_ch);
        self.dimmers[id] = Some(dimmer); //add error checking
    }

    /// Add an RGB color on the light at the provided index
    pub fn add_rgb(&mut self, id: usize,
                   red_ch: u16, green_ch: u16, blue_ch:u16){
        self.colors[id] = Some(
            ColorLight::rgb(self.lights[id].clone(), red_ch, green_ch, blue_ch)
        ); //add error checking
    }

    /// Add an RGBW color on the light at the provided index
    pub fn add_rgbw(&mut self, id: usize
                    , red: u16, green: u16, blue: u16, white: u16) {
        self.colors[id] = Some(
            ColorLight::rgbw(self.lights[id].clone(), red, green, blue, white)
        ); //add error checking
    }

    /// Sets the value of a channel of a light
    /// and wakes up the updater, if any.
    pub fn set(&mut self, id: usize, ch: u16, val: u8){
        self.lights[id].set(ChVal(ch, val));
        if let Some(ref c) = self.updater {
            c.update();
        }
    }

    pub fn set_some(&mut self, ids: &[usize], ch:u16, val: u8) {
        for id in ids {
            self.lights[*id].set(ChVal(ch, val));
        }
        if let Some(ref c) = self.updater {
            c.update();
        }
    }

    /// Returns true if the light identified by its id
    /// has a dimmer channel
    pub fn is_dimmered(&self, id: usize) -> bool {
        self.dimmers[id].is_some()
    }

    /// Returns true if the light identified by its id
    /// has color channels
    pub fn is_colored(&self, id:usize) -> bool {
        self.colors[id].is_some()
    }
    
    /// Start a fade-in transition of the dimmered light whose id is provided.
    /// `t` is the `Duration` of the full transition.
    pub fn fade_in_one(&mut self, id: usize, t: Duration)
                       -> Option<Transition> {
        if let Some(ref c) = self.updater {
            let s = c.get_arc();
            if let Some(ref mut d) = self.dimmers[id] {
                let mut d = d.clone();
                let t = d.fade_in(t);
                let arc = Arc::new(AtomicBool::default());
                let arc2 = arc.clone();
                return Some(Transition(thread::spawn( move || {
                    while d.fade_step(){
                        s.update();
                        thread::sleep(t);
                        if arc2.load(Ordering::Relaxed) == true {
                            break;
                        }
                    }
                }), arc))
            }
        }
        None
    }

    /// Start a fade-in transition of some of the dimmered lights together.
    /// `t` is the `Duration` of the full transition.
    /// `ids` is a slice containing the ids of all the dimmers to fade.
    pub fn fade_in_some(&mut self, ids: &[usize], t: Duration)
                        -> Option<Transition> {
        if let Some(ref c) = self.updater {
            let s = c.get_arc();
            let mut ds:Vec<Dimmer> = ids.iter().filter_map(|i| self.dimmers[*i].clone()).collect();
            let t = ds.iter_mut().map(|d| d.fade_in(t)).min().unwrap();
            let arc = Arc::new(AtomicBool::default());
            let arc2 = arc.clone();
            return Some(Transition(thread::spawn( move || {
                let mut cond = true;
                while cond {
                    cond = false;
                    for mut d in ds.iter_mut() {
                        if d.fade_step() == true {
                            cond = true;
                        }
                    }
                    s.update();
                    thread::sleep(t);
                    if arc2.load(Ordering::Relaxed) == true {
                        break;
                    }
                }
            }), arc))
        }
        None
    }

    /// Start a fade-in transition of all the dimmered lights together.
    /// `t` is the `Duration` of the full transition.
    pub fn fade_in_all(&mut self, t: Duration) -> Option<Transition> {
        if let Some(ref c) = self.updater {
            let s = c.get_arc();
            let mut ds:Vec<Dimmer> = self.dimmers.iter()
                .filter_map(|d| {
                    if let &Some(ref d) = d {
                        Some(d.clone())
                    } else {
                        None
                    }
                }).collect();
            let t = ds.iter_mut().map(|d| d.fade_in(t)).min().unwrap();
            let arc = Arc::new(AtomicBool::default());
            let arc2 = arc.clone();
            return Some(Transition(thread::spawn( move || {
                let mut cond = true;
                while cond {
                    cond = false;
                    for mut d in ds.iter_mut() {
                        if d.fade_step() == true {
                            cond = true;
                        }
                    }
                    s.update();
                    thread::sleep(t);
                    if arc2.load(Ordering::Relaxed) == true {
                        break;
                    }
                }
            }), arc))
        }
        None
    }

    /// Start a fade-in transition of the dimmered light whose id is provided.
    /// `t` is the `Duration` of the full transition.
    pub fn fade_out_one(&mut self, id:usize, t:Duration)
                        -> Option<Transition>{
        if let Some(ref c) = self.updater {
            let s = c.get_arc();
            if let Some(ref mut d) = self.dimmers[id] {
                let mut d = d.clone();
                let t = d.fade_out(t);
                let arc = Arc::new(AtomicBool::default());
                let arc2 = arc.clone();
                return Some(Transition(thread::spawn( move || {
                    while d.fade_step(){
                        s.update();
                        thread::sleep(t);
                        if arc2.load(Ordering::Relaxed) {
                            break;
                        }
                    }
                }), arc))
            }
        }
        None
    }

    /// Start a fade-out transition of some of the dimmered lights together.
    /// `t` is the `Duration` of the full transition.
    /// `ids` is a slice containing the ids of all the dimmers to fade.
    pub fn fade_out_some(&mut self, ids: &[usize], t: Duration)
                        -> Option<Transition> {
        if let Some(ref c) = self.updater {
            let s = c.get_arc();
            let mut ds:Vec<Dimmer> = ids.iter().filter_map(|i| self.dimmers[*i].clone()).collect();
            let t = ds.iter_mut().map(|d| d.fade_out(t)).min().unwrap();
            let arc = Arc::new(AtomicBool::default());
            let arc2 = arc.clone();
            return Some(Transition(thread::spawn( move || {
                let mut cond = true;
                while cond {
                    cond = false;
                    for mut d in ds.iter_mut() {
                        if d.fade_step() {
                            cond = true;
                        }
                    }
                    s.update();
                    thread::sleep(t);
                    if arc2.load(Ordering::Relaxed){
                        break;
                    }
                }
            }), arc))
        }
        None
    }

    /// Start a fade-out transition of all the dimmered lights together.
    /// `t` is the `Duration` of the full transition.
    pub fn fade_out_all(&mut self, t: Duration) -> Option<Transition> {
        if let Some(ref c) = self.updater {
            let s = c.get_arc();
            let mut ds:Vec<Dimmer> = self.dimmers.iter()
                .filter_map(|d| {
                    if let &Some(ref d) = d {
                        Some(d.clone())
                    } else {
                        None
                    }
                }).collect();
            let t = ds.iter_mut().map(|d| d.fade_out(t)).min().unwrap();
            let arc = Arc::new(AtomicBool::default());
            let arc2 = arc.clone();
            return Some(Transition(thread::spawn( move || {
                let mut cond = true;
                while cond {
                    cond = false;
                    for mut d in ds.iter_mut() {
                        if d.fade_step(){
                            cond = true;
                        }
                    }
                    s.update();
                    thread::sleep(t);
                    if arc2.load(Ordering::Relaxed) {
                        break;
                    }
                }
            }), arc))
        }
        None
    }

    pub fn fade_to_value_one(&mut self, id:usize , value:u8, t:Duration) -> Option<Transition>{
        if let Some(ref c) = self.updater {
            let s = c.get_arc();
            if let Some(ref mut d) = self.dimmers[id] {
                let mut d = d.clone();
                let t = d.fade_to_value(value, t);    
                let arc = Arc::new(AtomicBool::default());
                let arc2= arc.clone();
                return Some(Transition(thread::spawn(move || {
                    while d.fade_step() {
                        s.update();
                        thread::sleep(t);
                        if arc2.load(Ordering::Relaxed) {
                            break;
                        }
                    }
                }), arc))
            }
        }
        None
    }

    pub fn fade_to_value_some(&mut self, ids:&[usize] , value:u8, t:Duration) -> Option<Transition>{
        if let Some(ref c) = self.updater {
            let s = c.get_arc();
            let mut ds:Vec<Dimmer> = ids.iter().filter_map(|i| self.dimmers[*i].clone()).collect();
            let t = ds.iter_mut().map(|d| d.fade_to_value(value, t)).min().unwrap();
            let arc = Arc::new(AtomicBool::default());
            let arc2 = arc.clone();
            return Some(Transition(thread::spawn( move || {
                let mut cond = true;
                while cond {
                    cond = false;
                    for mut d in ds.iter_mut() {
                        if d.fade_step() {
                            cond = true;
                        }
                    }
                    s.update();
                    thread::sleep(t);
                    if arc2.load(Ordering::Relaxed){
                        break;
                    }
                }
            }), arc))
        }
        None
    }

    pub fn fade_to_value_all(&mut self, value:u8, t:Duration) -> Option<Transition>{
        if let Some(ref c) = self.updater {
            let s = c.get_arc();
            let mut ds:Vec<Dimmer> = self.dimmers.iter()
                .filter_map(|d| {
                    if let &Some(ref d) = d {
                        Some(d.clone())
                    } else {
                        None
                    }
                }).collect();
            let t = ds.iter_mut().map(|d| d.fade_to_value(value, t)).min().unwrap();
            let arc = Arc::new(AtomicBool::default());
            let arc2 = arc.clone();
            return Some(Transition(thread::spawn( move || {
                let mut cond = true;
                while cond {
                    cond = false;
                    for mut d in ds.iter_mut() {
                        if d.fade_step(){
                            cond = true;
                        }
                    }
                    s.update();
                    thread::sleep(t);
                    if arc2.load(Ordering::Relaxed) {
                        break;
                    }
                }
            }), arc))
        }
        None
    }

    ///Set the color of the light identified by id
    pub fn set_color_one(&mut self, id:usize, red: u8, green: u8, blue:u8) {
        self.colors[id].as_mut().unwrap().set_color(red, green, blue);
        if let Some(ref c) = self.updater {
            c.update();
        }
    }

    ///Set the color of the lights identified by the id's in the provided slice.
    pub fn set_color_some(&mut self, ids: &[usize], red: u8, green: u8, blue:u8) {
        for id in ids {
            self.colors[*id].as_mut().unwrap().set_color(red, green, blue);
        }
        if let Some(ref c) = self.updater {
            c.update();
        }
    }

    //Set the color of all the color lights in the universe
    pub fn set_color_all(&mut self, red: u8, green: u8, blue:u8){
        for mut c in self.colors.iter_mut().filter_map(|a| {
            if let &mut Some(ref mut b) = a {
                Some(b)
            } else {
                None
            }
        }) {
            c.set_color(red, green, blue);
        }
        if let Some(ref c) = self.updater {
            c.update();
        }
    }
  
    ///Set the color of the light identified by id
    pub fn fade_color_one(&mut self, id:usize, red: u8, green: u8, blue:u8, t: Duration)
                          -> Option<Transition> {
        if let Some(ref c) = self.updater {
            let s = c.get_arc();
            if let Some(ref mut c) = self.colors[id] {
                let mut c = c.clone();
                let t = c.fade_to_color(red, green, blue, t);
                let arc = Arc::new(AtomicBool::default());
                let arc2 = arc.clone();
                return Some(Transition(thread::spawn(move || {
                    while c.fade_step() {
                        s.update();
                        thread::sleep(t);
                        if arc2.load(Ordering::Relaxed) {
                            break;
                        }
                    }
                }), arc))
            }
        }
        None
    }

    ///Set the color of the lights identified by the id's in the provided slice.
    pub fn fade_color_some(&mut self, ids: &[usize], red: u8, green: u8, blue:u8, t: Duration)
                           -> Option<Transition> {
        if let Some(ref c) = self.updater {
            let s = c.get_arc();
            let mut cs:Vec<ColorLight> = ids.iter().filter_map(|i| self.colors[*i].clone()).collect();
            let t = cs.iter_mut().map(|c| c.fade_to_color(red, green, blue, t)).min().unwrap();
            let arc = Arc::new(AtomicBool::default());
            let arc2 = arc.clone();
            return Some(Transition(thread::spawn(move || {
                let mut cond = true;
                while cond {
                    cond = false;
                    for mut c in cs.iter_mut() {
                        if c.fade_step() {
                            cond = true;
                        }
                    }
                    s.update();
                    thread::sleep(t);
                    if arc2.load(Ordering::Relaxed) {
                        break;
                    }
                }
            }), arc))
        }
        None
    }

    //Set the color of all the color lights in the universe
    pub fn fade_color_all(&mut self, red: u8, green: u8, blue:u8, t: Duration)
                          -> Option<Transition> {
        if let Some(ref c)  = self.updater {
            let s = c.get_arc();
            let mut cs:Vec<ColorLight> = self.colors.iter().filter_map(|a| {
                if let &Some(ref b) = a {
                    Some(b.clone())
                } else {
                    None
                }
            }).collect();
            let t = cs.iter_mut().map(|c| c.fade_to_color(red, green, blue, t)).min().unwrap();
            let arc = Arc::new(AtomicBool::default());
            let arc2 = arc.clone();
            return Some(Transition(thread::spawn( move || {
                let mut cond = true;
                while cond {
                    cond = false;
                    for mut c in cs.iter_mut() {
                        if c.fade_step() {
                            cond = true;
                        }
                    }
                    s.update();
                    thread::sleep(t);
                    if arc2.load(Ordering::Relaxed) {
                        break;
                    }
                }
            }), arc))
        }
        None
    }  

    /// Turns off all the dimmered lights istantly
    pub fn go_bo(&mut self){
        for mut d in self.dimmers.iter().filter_map(|d|{
            if let &Some(ref d) = d {
                Some(d.clone())
            } else {
                None
            }
        }) {
            d.black_out();
        }
        if let Some(ref u) = self.updater {
            u.update();
        }
    }

    pub fn make_scene(&self) -> Vec<ChVal> {
        self.lights.iter().flat_map(|l| l.ch_vals()).collect()
    }

    pub fn apply_scene(&self, scene: &[ChVal]) {
        //this will be slow. I know that in the scene all channels of a light are contiguous
        for &ChVal(ch, val) in scene.iter() {
            //println!("applying scene: channel {}, value {}", ch, val);
            //println!("{:?}", self.used_chs);
            let light = self.used_chs.range(ch as u64, ch as u64)
                .next()
                .unwrap().1;
            let light = &self.lights[*light];
            light.set(ChVal(ch,val));
        }
    }
}

impl Drop for Universe {
    /// `drop`s this universe and stops
    /// the associated updater thread if present.
    fn drop(&mut self){
        if let Some(a) = self.updater.take() {
            a.stop();
        }
    }
}
