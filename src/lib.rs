/*
Copyright 2017 Gianmarco Garrisi

This file is part of LCS.

LCS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LCS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LCS.  If not, see <http://www.gnu.org/licenses/>. */

extern crate serial;
extern crate ordermap;
extern crate theban_interval_tree;
extern crate memrange;
extern crate priority_queue;
extern crate serde;

pub mod dmxsystem;
pub mod cache;
pub mod tools;

pub use cache::Cache;

///A couple channel, value
#[derive(Copy, Clone, Debug, Hash, PartialEq, Eq)]
pub struct ChVal(pub u16, pub u8);

use std::thread::JoinHandle;
use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};
pub struct Transition(JoinHandle<()>, Arc<AtomicBool>);

impl Transition{
    /// Stop a started transition istantly
    pub fn stop(self){
        self.1.store(true, Ordering::Relaxed);
        self.0.join().unwrap();
    }
}
