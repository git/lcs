/* Copyright 2017 Gianmarco Garrisi

This file is part of LCS.

LCS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LCS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LCS.  If not, see <http://www.gnu.org/licenses/>. */

extern crate lcs;
extern crate gtk;
extern crate serial;

use std::rc::Rc;
use std::sync::Arc;
use std::cell::{Cell, RefCell};
use std::error::Error;
use std::time::Duration;
use std::collections::HashMap;

use gtk::prelude::*;
use gtk::{
    Widget, ToolButton, Button, Dialog, Box, ListStore, TreeSelection,
    Builder, ApplicationWindow, MenuItem, Grid, ButtonBox, Value,
    Entry, Label, ComboBoxText, Orientation, Statusbar, Adjustment,
    MessageDialog, Scale, Paned, Notebook, ColorChooserWidget,
    SpinButton, ToggleButton, MessageDialogExt, Container
};

use lcs::dmxsystem::Universe;
use lcs::dmxsystem::devs::SimpleLight;
use lcs::cache::{Cache, CachingPolicy};
use lcs::tools::scenes::*;

/* the following constants contains the glade sources as strings.
Change the argument of include_str! if the path of the files changes */
const GLADE_SRC: &'static str=include_str!("GUI.glade");
const ADD_LIGHT_DIALOG_SRC: &'static str=include_str!("add_light_dialog.glade");
const ADD_SCENE_DIALOG_SRC: &'static str=include_str!("add_scene_dialog.glade");
const ELEMENTS_PER_ROW: u16 = 8;

/* The clone macro from gtk-rs */
macro_rules! clone {
    (@param _) => ( _ );
    (@param $x:ident) => ( $x );
    ($($n:ident),+ => move || $body:expr) => (
        {
            $( let $n = $n.clone(); )+
                move || $body
        }
    );
    ($($n:ident),+ => move |$($p:tt),+| $body:expr) => (
        {
            $( let $n = $n.clone(); )+
                move |$(clone!(@param $p),)+| $body
        }
    );
}

type ScenesContainer = Rc<RefCell<HashMap<String, Scene>>>;
type SlidCache = Rc<RefCell<Cache<String, Notebook>>>;

fn main() {

    let u = Rc::new(RefCell::new(Universe::new()));
    let fullscreen = Rc::new(Cell::new(false));
    let sliders_cache: SlidCache = Rc::new(RefCell::new(Cache::new(16,
                                                                   CachingPolicy::LeastFrequentlyUsed)));
    let before_bo:Rc<RefCell<Option<Scene>>> = Rc::new(RefCell::new(None));
    let scenes: ScenesContainer = Rc::new(RefCell::new(HashMap::new()));

    if gtk::init().is_err(){
        panic!("Fatal error! Unable to initialize Graphic Interface")
    }

    let builder = Builder::new();
    let result = builder.add_from_string(GLADE_SRC);
    match result {
        Ok(_) => {}
        Err(message) => println!("{}", message)
    }

    let window: ApplicationWindow = builder.get_object("main").unwrap();
    /* Status bar*/
    let stat: Statusbar = builder.get_object("stat").unwrap();

    /* About dialog */
    let tmp_menuitem: MenuItem = builder.get_object("about_menu").unwrap();
    let tmp_dialog: Dialog = builder.get_object("about").unwrap();
    tmp_dialog.connect_close(clone!(
        tmp_dialog => move |_|{
            tmp_dialog.hide();
        }
    ));
    tmp_menuitem.connect_activate(move |_| {
        tmp_dialog.run();
        tmp_dialog.hide();
    });

    let tmp_menuitem: MenuItem =  builder.get_object("gtk-fullscreen").unwrap();
    tmp_menuitem.connect_activate(clone!(
        window => move |_| {
            toggle_fullscreen(fullscreen.clone(), window.clone());
        }
    ));


    let lights_list: ListStore = builder.get_object("light_list").unwrap();
    let lights_select: TreeSelection =
        builder.get_object("lights_select").unwrap();
    let paned: Paned = builder.get_object("paned").unwrap();

    /* when in the side pane a light is selected,
    show a slider for each channel */
    /* then add color selection for rgb lights, etc...  */
    lights_select.connect_changed(clone!(
        u, paned, sliders_cache => move |ls| {
            light_selected(ls.clone(), u.clone(),
                           paned.clone(), sliders_cache.clone());
        }
    ));

    /* Add light dialog */
    let tmp_button: ToolButton = builder.get_object("add_light_btn").unwrap();
    tmp_button.connect_clicked(clone!(
        u, stat, window, lights_list => move |_| {
            add_light(&window, stat.clone(), u.clone(), lights_list.clone());
        }
    ));
    let tmp_menuitem: MenuItem = builder.get_object("add_light_btn_menu").unwrap();
    tmp_menuitem.connect_activate(clone!(
        u, stat, window, lights_list => move |_| {
            add_light(&window, stat.clone(), u.clone(), lights_list.clone());
        }
    ));

    let scene_list: ListStore = builder.get_object("scene_list").unwrap();
    let scene_select: TreeSelection = builder.get_object("scenes_select").unwrap();
    
    /* Add scene dialog*/
    let tmp_menuitem: MenuItem = builder.get_object("add_scene_btn_menu").unwrap();
    tmp_menuitem.connect_activate(clone!(
        scenes, u, stat, window => move |_| {
            add_scene(&window, stat.clone(), u.clone(), scene_list.clone(), scenes.clone());
        }
    )); 

    /* Scene selection */
    scene_select.connect_changed(clone!(
        u, scenes, paned, lights_select => move |selection| {
            let (model, selection) = selection.get_selected().unwrap();
            let scene_name = model.get_value(&selection, 0);
            let scene_name:String = scene_name.get().unwrap();
            let (paths, model) = lights_select.get_selected_rows();
            let selection = model.get_iter(&paths[0]).unwrap();
            let light_name: String = model.get_value(&selection, 0).get().unwrap();
            scenes.borrow().get(&scene_name).unwrap().apply(&(*u.borrow()));
            update_sliders_view(paned.clone(), u.borrow().get_light(light_name.as_str()).unwrap());
        }
    ));

    /* BlackOut Button */
    // create a temporary scene and remember the state,
    // so that, when black out is deactivated, the previous state is restored
    // a toggle button could be used
    let bo_bttn: ToolButton = builder.get_object("black_out").unwrap();
    bo_bttn.connect_clicked(clone!(
        u, paned, before_bo, lights_select => move |_| {
            let mut before_bo = before_bo.borrow_mut();
            if let Some(tmp_scene) = before_bo.take() {
                tmp_scene.apply(&(*u.borrow()));
            } else {
                *before_bo = Some(Scene::new(&(*u.borrow())));
                u.borrow_mut().go_bo();
            }
            let (paths, model) = lights_select.get_selected_rows();
            let selection = model.get_iter(&paths[0]).unwrap();
            let light_name: String = model.get_value(&selection, 0).get().unwrap();
            update_sliders_view(paned.clone(), u.borrow().get_light(light_name.as_str()).unwrap());
        }
    ));

    /* quit */
    let tmp_menuitem: MenuItem = builder.get_object("quit_menuitem").unwrap();
    tmp_menuitem.connect_activate(|_| {
        gtk::main_quit();
    });

    /* "splash" screen */
    let tmp_dialog: Dialog = builder.get_object("welcome").unwrap();
    tmp_dialog.connect_close(clone!(
        tmp_dialog => move |_| {
            tmp_dialog.destroy();
        }
    ));

    /* Start button */
    let start_bttn: ToolButton = builder.get_object("start").unwrap();
    let error: MessageDialog = builder.get_object("start_error").unwrap();
    error.connect_close(clone!(
        error => move |_| {
            error.hide();
        }
    ));
    error.connect_delete_event(clone!(
        error => move |_,_| {
            error.hide();
            Inhibit(false)
        }
    ));

    start_bttn.connect_clicked(clone!(
        u, error => move |_| {
            if let Err(e) = u.borrow_mut()
                .start(serial::PortSettings::default()){
                    error.set_property_secondary_text(Some(e.description()));
                    error.run();
                    error.hide();
                };
        }
    ));

    /*Stop button*/
    let stop_bttn: ToolButton = builder.get_object("stop").unwrap();
    let error: MessageDialog = builder.get_object("stop_error").unwrap();
    error.connect_close(clone!(
        error => move |_| {
            error.hide();
        }
    ));
    error.connect_delete_event(clone!(
        error => move |_,_| {
            error.hide();
            Inhibit(false)
        }
    ));
    stop_bttn.connect_clicked(clone!(
        u, error => move |_| {
            if let Err(e) = u.borrow_mut().stop() {
                error.set_property_secondary_text(Some(e.as_str()));
                error.run();
                error.hide();
            };
        }
    ));

    window.connect_delete_event(|_,_| {
        gtk::main_quit();
        Inhibit(false)
    });

    window.show_all();
    tmp_dialog.run();
    tmp_dialog.destroy();

    let contid = stat.get_context_id("Ready");
    stat.push(contid, "Ready");
    gtk::main();
}

fn toggle_fullscreen(fullscreen: Rc<Cell<bool>>, window: ApplicationWindow) {
    if fullscreen.get() {
        fullscreen.set(false);
        window.unfullscreen();
    } else {
        fullscreen.set(true);
        window.fullscreen();
    }
}

fn update_sliders_view(paned: Paned, light: Arc<SimpleLight>){
    if let Some(nb) = paned.get_child2(){
        let nb = nb.downcast::<Notebook>().unwrap();
        let tmp = nb.get_children()[0].clone()
            .downcast::<Container>()
            .unwrap()
            .get_children();
        let sliders = tmp.iter().rev()
            .map(|a| a.clone().downcast::<Scale>().unwrap());
        for (i, s) in sliders.enumerate() {
            s.set_value(light.get_value(i as u16) as f64);
        }
    }
}

fn light_selected(ls: TreeSelection, u: Rc<RefCell<Universe>>, paned: Paned,
                  sliders_cache: SlidCache){
    let (rows, tm) = ls.get_selected_rows();
    if rows.len() == 1 {
        let name: String =
            tm.get_value(&(tm.get_iter(&rows[0]).unwrap()), 0).get().unwrap();
        let mut nb =
            if let Some(nb) = sliders_cache.borrow_mut().get(name.as_str()) {
                Some(nb.clone())
            } else {
                None
            };
        if nb == None {
            let noteb = Notebook::new();
            let g = Grid::new();
            let light = u.borrow().get_light(&name).unwrap();
            let id = light.get_id();
            let n = light.get_number_of_chs();
            let start = light.get_first_ch();
            let mut v = Vec::new();
            for i in start..start+n {
                // add sliders to the grid
                let s =
                    Scale::new_with_range(Orientation::Vertical,
                                          0.0, 255.0, 1.0);
                s.set_inverted(true);
                // when a value is changed,
                // change the value from the universe
                s.connect_change_value(clone!(
                    u => move |_,_,value| {
                        u.borrow_mut().set(id, i, value as u8);
                        Inhibit(false)
                    }
                ));
                g.attach(&s, ((i-start) % ELEMENTS_PER_ROW) as i32,
                         ((i-start) / ELEMENTS_PER_ROW) as i32, 1, 1);
                v.push(s);
            }
            noteb.append_page(&g, Some(&Label::new("Raw channels")));
            // Update the position of cursors when the Raw channels page is showed again.
            noteb.connect_switch_page(clone!(
                light => move |_,_, p| {
                    if p == 0 {
                        println!("Here!");
                        for (i, s) in v.iter().enumerate() {
                            let val = light.get_value(i as u16);
                            s.set_value(val as f64);
                        }
                    }
                }
            ));
            g.set_column_homogeneous(true);
            g.set_row_homogeneous(true);
            if u.borrow().is_colored(id) {
                let gbox = Box::new(Orientation::Vertical, 10);
                let chooser = ColorChooserWidget::new();
                gbox.add(&chooser);
                let button = Button::new_with_label("Palette");
                button.connect_clicked(clone!(
                    chooser => move |_| {
                        chooser.set_property_show_editor(false);
                    }
                ));
                let hbox = Box::new(Orientation::Horizontal, 10);
                hbox.add(&button);
                let button = Button::new_with_label("Select");
                hbox.add(&button);
                gbox.add(&hbox);
                let hbox = Box::new(Orientation::Horizontal, 10);
                let button = ToggleButton::new_with_label("Fade");
                hbox.add(&button);
                let spinb = SpinButton::new_with_range(0.0, 10000.0, 0.1);
                hbox.add(&Label::new("Fading time:"));
                hbox.add(&spinb);
                hbox.add(&Label::new("s"));
                chooser.connect_color_activated(clone!(
                    u, button, spinb => move |_, rgba| {
                        if button.get_active() {
                            let t = spinb.get_value();
                            let t = Duration::new(t.trunc() as u64,
                                               (t.fract()*1000000000.0) as u32);
                            u.borrow_mut()
                                .fade_color_one(id,
                                                (rgba.red*255.0) as u8,
                                                (rgba.green*255.0) as u8,
                                                (rgba.blue*255.0) as u8,
                                                t);
                        } else {
                            u.borrow_mut()
                                .set_color_one(id,
                                               (rgba.red*255.0) as u8,
                                               (rgba.green*255.0) as u8,
                                               (rgba.blue*255.0) as u8);
                        }
                    }
                ));
                gbox.add(&hbox);
                noteb.append_page(&gbox, Some(&Label::new("Color")));
            }
            if u.borrow().is_dimmered(id) {
                let gbox = Box::new(Orientation::Vertical, 10);
                let s = Scale::new_with_range(Orientation::Horizontal,
                                              0.0, 100.0, 0.001);
                gbox.add(&s);
                let hbox = Box::new(Orientation::Horizontal, 10);
                let spinb = SpinButton::new_with_range(0.0, 10000.0, 0.1);
                s.connect_change_value(clone!(
                    u, spinb => move |_,_,value| {
                        let t = spinb.get_value();
                        let t = Duration::new(t.trunc() as u64,
                                              (t.fract()*1000000000.0) as u32);
                        u.borrow_mut().fade_to_value_one(id, value as u8, t);
                        Inhibit(false)
                    }
                ));
                let button = Button::new_with_label("Fade in");
                button.connect_clicked(clone!(
                    u, spinb => move |_| {
                        let t = spinb.get_value();
                        let t = Duration::new(t.trunc() as u64,
                                              (t.fract()*1000000000.0) as u32);
                        u.borrow_mut().fade_in_one(id, t);
                    }
                ));
                hbox.add(&button);
                let button = Button::new_with_label("Fade out");
                button.connect_clicked(clone!(
                    u, spinb => move |_| {
                        let t = spinb.get_value();
                        let t = Duration::new(t.trunc() as u64,
                                              (t.fract()*1000000000.0) as u32);
                        u.borrow_mut().fade_out_one(id, t);
                    }
                ));
                hbox.add(&button);
                hbox.add(&Label::new("Fading time:"));
                hbox.add(&spinb);
                hbox.add(&Label::new("s"));
                gbox.add(&hbox);
                noteb.append_page(&gbox, Some(&Label::new("Dimmer")));
            }
            sliders_cache.borrow_mut().insert(name.clone(), noteb.clone());
            nb = Some(noteb);
        };
        let nb = nb.unwrap();
        if let Some(old_nb) = paned.get_child2(){
            paned.remove(&old_nb);
        }
        paned.add2(&nb);
        nb.set_hexpand(true);
        nb.show_all();
    }
}

fn add_scene(main_window: &ApplicationWindow,
             status_bar: Statusbar,
             universe: Rc<RefCell<Universe>>,
             list: ListStore,
             scenes: ScenesContainer) {
    let builder = Builder::new();
    match builder.add_from_string(ADD_SCENE_DIALOG_SRC) {
        Ok(_) => {},
        Err(e) =>  panic!("Error in glade source: {}", e),
    }

    let add_dialog: Dialog = builder.get_object("dialog").unwrap();
        

    add_dialog.set_transient_for(Some(main_window));

    //connect all possible ways to close window
    add_dialog.connect_close(clone!(
        add_dialog => move |_| {
            add_dialog.destroy();
        }
    ));
    builder.get_object::<Button>("cancel").unwrap()
        .connect_clicked(clone!(
            add_dialog => move |_| {
                add_dialog.destroy();
            }
        ));
    add_dialog.connect_delete_event(clone!(
        add_dialog => move |_,_|{
            add_dialog.destroy();
            Inhibit(false)
        }
    ));

    //connect ok button
    let ok_btn: Button = builder.get_object("ok").unwrap();
    ok_btn.connect_clicked(clone!(
        universe, list, scenes, builder, add_dialog => move |_| {
            let name = builder.get_object::<Entry>("name")
                .unwrap()
                .get_text();
            match name {
                None => {
                    let error: Dialog = builder.get_object("name_null").unwrap();
                    error.run();
                    error.hide();
                }
                Some(name) => {
                    if name.len() == 0 {
                        let error: Dialog = builder.get_object("name_null").unwrap();
                        error.run();
                        error.hide();
                        return;
                    }
                    let new_s = Scene::new(&(*universe.borrow()));
                    if scenes.borrow().contains_key(&name) {
                        let error:Dialog = builder.get_object("name_not_unique").unwrap();
                        error.run();
                        error.hide();
                        return;
                    }
                    scenes.borrow_mut().insert(name.clone(), new_s);
                    let contid = status_bar.get_context_id("Add scene");
                    status_bar.push(contid, "Scene crated");
                    let iter = list.append();
                    let v = Value::from(Some(&(*name)));
                    list.set_value(&iter, 0, &v);
                    add_dialog.destroy();
                }
            }
        }
    ));

    add_dialog.run();
}

fn add_light(main_window: &ApplicationWindow,
             status_bar: Statusbar,
             universe: Rc<RefCell<Universe>>,
             list: ListStore){
    let builder = Builder::new();
    builder.add_from_string(ADD_LIGHT_DIALOG_SRC).unwrap();

    let add_dialog: Dialog = builder.get_object("add_light_dialog").unwrap();

    //add transient parent
    add_dialog.set_transient_for(Some(main_window));

    //connect all possible ways to close window
    add_dialog.connect_close(clone!(
        add_dialog => move |_| {
            add_dialog.destroy();
        }
    ));
    builder.get_object::<Button>("add_light_cancel").unwrap()
        .connect_clicked(clone!(
            add_dialog => move |_| {
                add_dialog.destroy();
            }
        ));
    add_dialog.connect_delete_event(clone!(
        add_dialog => move |_,_|{
            add_dialog.destroy();
            Inhibit(false)
        }
    ));

    //connect ok button to next step
    let first_ch_adj: Adjustment = builder.get_object("adjustment1").unwrap();
    let num_of_chs: Adjustment = builder.get_object("adjustment2").unwrap();
    let name: Entry = builder.get_object("light_name").unwrap();
    let ok_bttn: Button = builder.get_object("add_light_ok").unwrap();
    ok_bttn.connect_clicked(clone!(
        add_dialog, name, builder, list => move |_| {
            let first_channel = first_ch_adj.get_value() as u16;
            let number_of_channels = num_of_chs.get_value() as u16;
            let name = Rc::new(
                if let Some(name) = name.get_text() {
                    //println!("{:?}", name);
                    if let Some(_) = universe.borrow().get_index(&name) {
                        //error dialog (name must be unique) and
                        // go to prev step (actually, don't go on)
                        //println!("Name not unique!");
                        let error_d:Dialog =
                            builder.get_object("name_not_unique").unwrap();
                        //connect all possible ways to close window
                        error_d.connect_close(clone!(
                            error_d => move |_| {
                                error_d.hide();
                            }
                        ));
                        error_d.connect_delete_event(clone!(
                            error_d => move |_,_|{
                                error_d.hide();
                                Inhibit(false)
                            }
                        ));
                        error_d.run();
                        error_d.hide();
                        return;
                    } else {
                        name
                    }
                } else {
                    //error dialog and go to prev step
                    return; //??
                }
            );
            //hide window
            //tmp_d.hide();
            //clear window
            if let Some(ref child) = add_dialog.get_child(){
                add_dialog.remove(child);
                child.destroy();
            }
            //draw next phase
            let mut decorations =
                Vec::with_capacity(number_of_channels as usize);
            let g = Grid::new();
            let b = Box::new(Orientation::Vertical, 10);
            let butt_box = ButtonBox::new(Orientation::Horizontal);
            let ok_button = Button::new_from_stock("gtk-ok");
            let cancel_button =
                Button::new_from_stock("gtk-cancel");
            cancel_button.connect_clicked(
                clone!(add_dialog => move |_| {add_dialog.destroy()}));
            butt_box.add(&cancel_button);
            butt_box.add(&ok_button);
            butt_box.set_layout(gtk::ButtonBoxStyle::End);
            for i in 0..number_of_channels as i32{
                let ch_name =
                    Label::new(Some(format!("Channel {}", i+1).as_str()));
                g.attach(&ch_name,    0, i, 1, 1);
                let decoration = ComboBoxText::new();
                decoration.append_text("Dimmer coarse");
                decoration.append_text("Dimmer fine");
                decoration.append_text("Red");
                decoration.append_text("Green");
                decoration.append_text("Blue");
                g.attach(&decoration, 1, i, 1, 1);
                decorations.push(decoration);
            }
            let decorations = Rc::new(decorations);
            ok_button.connect_clicked(clone!(
                add_dialog, status_bar, universe, name, builder, list =>
                    move |_| {
                        let mut dimmer = (None, None);
                        let mut rgb = (None, None, None);
                        for (i, decoration) in decorations.iter().enumerate() {
                            if let Some(string) = decoration.get_active_text() {
                                match string.as_str() {
                                    "Dimmer coarse" => dimmer.0 = Some(i),
                                    "Dimmer fine"   => dimmer.1 = Some(i),
                                    "Red"           => rgb.0 = Some(i),
                                    "Green"         => rgb.1 = Some(i),
                                    "Blue"          => rgb.2 = Some(i),
                                    &_ => {} /* impossible */
                                }
                            }
                        }
                        if (rgb.0.is_some() ||
                            rgb.1.is_some() ||
                            rgb.2.is_some()) &&
                            (rgb.0.is_none() ||
                             rgb.1.is_none() ||
                             rgb.2.is_none())
                        {
                            /* ERROR dialog:
                            RGB light only if all three colors are selected */
                            //println!("One of Red, Green, Blue missing");
                            let error_d:Dialog =
                                builder.get_object("rgb_without_color")
                                .unwrap();
                            //connect all possible ways to close window
                            error_d.connect_close(clone!(
                                error_d => move |_| {
                                    error_d.hide();
                                }
                            ));
                            error_d.connect_delete_event(clone!(
                                error_d => move |_,_|{
                                    error_d.hide();
                                    Inhibit(false)
                                }
                            ));
                            error_d.run();
                            error_d.hide();
                            return;
                        }
                        if dimmer.1.is_some() && dimmer.0.is_none() {
                            /* ERROR dialog:
                            fine dimmer only if coarse is present */
                            //println!("Fine without coarse!");
                            let error_d:Dialog =
                                builder.get_object("fine_without_coarse")
                                .unwrap();
                            //connect all possible ways to close window
                            error_d.connect_close(clone!(
                                error_d => move |_| {
                                    error_d.hide();
                                }
                            ));
                            error_d.connect_delete_event(clone!(
                                error_d => move |_,_|{
                                    error_d.hide();
                                    Inhibit(false)
                                }
                            ));
                            error_d.run();
                            error_d.hide();
                            return;
                        }
                        let l =
                            if let Ok(mayl) = universe.borrow_mut()
                            .add_light((*name).clone(),
                                       first_channel, number_of_channels)
                        {
                            mayl
                        } else {
                            /* Error dialog */
                            println!("ERROR: maybe some light channels are intersecting?");
                            add_dialog.destroy();
                            //FIXME: go to previous step instead of destroying
                            return;
                        };
                        if rgb.0.is_some() &&
                            rgb.1.is_some() &&
                            rgb.2.is_some() {
                                universe.borrow_mut()
                                    .add_rgb(l, rgb.0.unwrap() as u16,
                                             rgb.1.unwrap() as u16,
                                             rgb.2.unwrap() as u16);
                            }
                        if dimmer.0.is_some() && dimmer.1.is_some() {
                            unimplemented!()
                        } else if dimmer.0.is_some() {
                            universe.borrow_mut()
                                .add_dimmer(l, dimmer.0.unwrap() as u16);
                        }
                        //println!("{:?}", universe );
                        let contid = status_bar.get_context_id("Add light");
                        status_bar.push(contid, "Light added");
                        let iter = list.append();
                        let v = Value::from(Some(&(*name)));
                        //println!("{:?}", v);
                        list.set_value(&iter, 0, &v);
                        add_dialog.destroy();
                    }
            ));
            let vp = gtk::Viewport::new(None, None);
            vp.add(&g);
            let sw = gtk::ScrolledWindow::new(None, None);
            b.add(&vp);
            b.add(&butt_box);
            sw.add(&b);
            add_dialog.add(&sw);
            //show window
            add_dialog.show_all();
            add_dialog.run();
        }
    ));

    add_dialog.run();
}
