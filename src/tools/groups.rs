/*
Copyright 2017 Gianmarco Garrisi

This file is part of LCS.

LCS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LCS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LCS.  If not, see <http://www.gnu.org/licenses/>. */
use std::time::Duration;

use ::dmxsystem::Universe;
use ::Transition;

pub struct Group {
    lights: Vec<usize>,
    dimmers: bool,
    colors: bool,
}

impl Group {
    pub fn new() -> Group {
        Group{ lights: Vec::new(),
               dimmers:true,
               colors:true }
    }

    pub fn add(&mut self, light: usize, universe: &Universe) {
        self.lights.push(light);
        if self.dimmers{
            self.dimmers = universe.is_dimmered(light);
        }
        if self.colors {
            self.colors = universe.is_colored(light);
        }
    }

    pub fn add_some(&mut self, lights: &mut Vec<usize>, universe: &Universe) {
        self.lights.append(lights);
        if self.dimmers {
            self.dimmers = lights.iter()
                .all(|i| universe.is_dimmered(*i));
        }
        if self.colors {
            self.colors = lights.iter()
                .all(|i| universe.is_colored(*i));
        }
    }

    //pub fn remove(&mut self, light: usize) {
    //    self.lights.remove_item(light); //remove item is experimental (nightly)
    //}

    pub fn set(&self, universe: &mut Universe, ch: u16, value: u8) {
        universe.set_some(self.lights.as_slice(), ch, value)
    }

    pub fn fade_in(&self, universe: &mut Universe,  t: Duration) -> Option<Transition> {
        universe.fade_in_some(self.lights.as_slice(), t)
    }

    pub fn fade_out(&self, universe: &mut Universe, t: Duration) -> Option<Transition>  {
        universe.fade_out_some(self.lights.as_slice(), t)
    }

    pub fn fade_to_value(&self, universe: &mut Universe, value: u8, t: Duration) -> Option<Transition>  {
        universe.fade_to_value_some(self.lights.as_slice(), value, t)
    }

    pub fn set_color(&self, universe: &mut Universe, red: u8, green: u8, blue:u8) {
        universe.set_color_some(self.lights.as_slice(), red, green, blue)
    }

    pub fn fade_to_color(&self, universe: &mut Universe, red: u8, green: u8, blue:u8, t: Duration)  -> Option<Transition> {
        universe.fade_color_some(self.lights.as_slice(), red, green, blue, t)
    }
}
