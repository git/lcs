/*
Copyright 2017 Gianmarco Garrisi

This file is part of LCS.

LCS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LCS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LCS.  If not, see <http://www.gnu.org/licenses/>. */

use ::dmxsystem::universe::Universe;
use ::ChVal;

#[derive(Debug, Hash, PartialEq, Eq)]
pub struct Scene {
    state: Vec<ChVal>
}

impl Scene {
    pub fn new(universe: &Universe) -> Scene {
        Scene{
            state: universe.make_scene()
        }
    }

    pub fn apply(&self, universe: &Universe) {
        universe.apply_scene(self.state.as_slice())
    }
}

use std::time::Duration;
use std::thread;
#[derive(Debug)]
pub struct Sequence<'a> {
    scenes: Vec<&'a Scene>,
    timings: Vec<Duration>,
    repeat: bool
}

impl<'a> Sequence <'a> {
    pub fn new() -> Sequence<'a> {
        Sequence {
            scenes: Vec::new(),
            timings: Vec::new(),
            repeat: false
        }
    }

    pub fn add_scene(&mut self, scene:&'a Scene) {
        self.scenes.push(scene);
        self.timings.push(Duration::default());
    }

    pub fn remove_scene(&mut self, scene: usize) {
        self.scenes.remove(scene);
        self.timings.remove(scene);
    }

    pub fn set_timing(&mut self, scene: usize, timing: Duration) {
        self.timings[scene] = timing;
    }

    pub fn run(&self, universe: &Universe) {
        if self.repeat {
            for (scene, timing) in self.scenes.iter().zip(self.timings.iter()).cycle() {
                scene.apply(universe);
                thread::sleep(*timing);
            }
        } else {
            for (scene, timing) in self.scenes.iter().zip(self.timings.iter()) {
                scene.apply(universe);
                thread::sleep(*timing);
            }
        }
    }
}
